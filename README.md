# Getting started with Google Cloud (on Windows)


* [Google Cloud console](https://console.cloud.google.com/)


Free trial:

* [12 month, $300 free trial](https://cloud.google.com/free/docs/frequently-asked-questions)


Installing Google Cloud SDK on Windows:

* [Installing Cloud SDK](https://cloud.google.com/sdk/downloads)

If you have no python installed, or python 3 installed (regardless of whether you have python 2 on your system), then you will need to select "bundled python".
Cloud SDK requires python 2, version 2.7.x or later.


Once the SDK is installed,
try `gcloud --help`.




## Initialing SDK


* [Initializing Cloud SDK](https://cloud.google.com/sdk/docs/initializing)


    gcloud init --console-only






